<?php

$dbhost = "127.0.0.1";

// Edit the following values
$dbuser = "root"; //the mysql user
$dbpass = ""; //the mysql password
$dbname = "halsimplify";
$path = "/Users/ateeq-ahmed/work/tools/my-bash-scripts/Fetch_Profile_images/images/"; //the directory path to where you want to store your backups
$port = "3306";

//get the list of databases
$linkLive = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname, $port);
$result = mysqli_query($linkLive, "select id, CONCAT_WS(' ', employee_master.first_name, employee_master.last_name) as name from employee_master where status = 'ACTIVE';");

$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI0LCJpc3MiOiJodHRwczovL2hhbGVycC5jb20vb2F1dGgyL2xvZ2luL2NhbGxiYWNrIiwiaWF0IjoxNzExNjEyODU4LCJleHAiOjE3NDMxNzA0NTgsIm5iZiI6MTcxMTYxMjg1OCwianRpIjoiaDhMTnRnSXkxckFUNUFsUyJ9.l_oGpTxDe3Bp6nGLizt3d_plmqHRxU2KmWVz7GqjbBM';

$url = 'https://halsimplify.halerp.com/jwt/user/getUserProfileImage?for=EmployeeMaster&id=';

//iterate over the list of databases
while ($row = mysqli_fetch_row($result)) {

        $employeeId = $row[0];
        $employeeName = $row[1];
        $fileExt = 'png';
        $imagePath = "/Users/ateeq-ahmed/work/tools/my-bash-scripts/Fetch_Profile_images/images/". str_replace(' ', '_', $employeeName);

        $downloadUrl = $url . $employeeId . '&token=' . $token;

        // $image = file_get_contents($downloadUrl);
        $image = null;

        $ch = curl_init($downloadUrl); // such as http://example.com/example.xml
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $image = curl_exec($ch);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close($ch);

        if ($image) {

            // Extract extension from Content-Type (if available)
            $fileExt = explode("/", $contentType)[1] ?? $fileExt;

            $fileExt = '.'.$fileExt;
            
            echo "File extension: " . $fileExt;

            file_put_contents($imagePath . $fileExt, $image);

            echo('file saved successfully at : '. $imagePath . $fileExt . PHP_EOL);
        }
        else {

            echo('error getting file'. PHP_EOL);
        }
    
}